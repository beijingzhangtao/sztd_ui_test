FROM maven:3-jdk-8 as builder
MAINTAINER df1228@gmail.com
ARG MAVEN_OPTS="-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn"
ENV MAVEN_OPTS="${MAVEN_OPTS}"
ADD pom.xml pom.xml
ADD src src
RUN mvn clean package -B -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn
RUN cp target/LuckyFrameWeb.jar /tmp/LuckyFrameWeb.jar

FROM openjdk:8-jdk-alpine
COPY --from=builder  /tmp/LuckyFrameWeb.jar app.jar
EXPOSE 9008
ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} -jar /app.jar ${0} ${@}"]
