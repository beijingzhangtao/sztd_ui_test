package com.luckyframe.project.system.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.luckyframe.project.system.menu.domain.Menu;
import com.luckyframe.project.system.menu.service.IMenuService;
import com.luckyframe.project.system.user.domain.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.luckyframe.common.utils.ServletUtils;
import com.luckyframe.common.utils.StringUtils;
import com.luckyframe.framework.config.LuckyFrameConfig;
import com.luckyframe.framework.web.controller.BaseController;
import com.luckyframe.framework.web.domain.AjaxResult;

import java.util.List;

/**
 * 登录验证
 * 
 * @author ruoyi
 */
@Controller
public class LoginController extends BaseController
{
    @Autowired
    private LuckyFrameConfig lfConfig;
    @Autowired
    private IMenuService menuService;


    
    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response, ModelMap mmap)
    {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request))
        {
            return ServletUtils.renderString(response, "{\"code\":\"1\",\"msg\":\"未登录或登录超时。请重新登录\"}");
        }
        mmap.put("copyrightYear", lfConfig.getCopyrightYear());
        mmap.put("version", lfConfig.getVersion());
        return "login";
    }

    @PostMapping("/login")
    @ResponseBody
    public AjaxResult ajaxLogin(String username, String password, Boolean rememberMe)
    {
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
        Subject subject = SecurityUtils.getSubject();
        try
        {
            subject.login(token);            
            return success();
        }
        catch (AuthenticationException e)
        {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage()))
            {
                msg = e.getMessage();
            }
            return error(msg);
        }
    }

    @GetMapping("/loginGet")
    public String ajaxLoginGet(String username, String password,ModelMap mmap)
    {
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, true);
        Subject subject = SecurityUtils.getSubject();
        try
        {
            subject.login(token);
            System.out.println("username======"+username);
            BaseController.loginName=username;
            // 取身份信息
            User user = getSysUser();
            // 根据用户取出菜单
            List<Menu> menus = menuService.selectMenusByUser(user);
            mmap.put("menus", menus);
            mmap.put("user", user);
            mmap.put("copyrightYear", lfConfig.getCopyrightYear());
            mmap.put("version", lfConfig.getVersion());
            return "index";

        }
        catch (AuthenticationException e)
        {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage()))
            {
                msg = e.getMessage();
            }
            //return error(msg);
            return "/error/unauth";
        }
    }

    @GetMapping("/unauth")
    public String unauth()
    {
        return "/error/unauth";
    }
}
